import React, {Component} from 'react';
import Header from '../../components/Header/Header.js'
import ListItemNews from '../../components/ListItemNews/ListItemNews.js'
import Body from '../../components/Body/Body.js'
class HomePage extends Component {
  render(){
     return (
       <div>
          <Header />
          <ListItemNews />
          <Body />
       </div>
    );
  }
 
}

export default HomePage;
