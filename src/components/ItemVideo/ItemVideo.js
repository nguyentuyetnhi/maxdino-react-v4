import React, { Component } from 'react';
import videothumb from '../../assets/images/video-thumb.jpg';
import './ItemVideo.scss';

class ItemVideo extends Component {
  render(){
       return (

        <div className="item-video">
          <div className="item-video-img">
            <img src={this.props.img} alt="video" />
            <a href="#"><span className="icon-icon" /></a>
          </div>
          <div className="item-video-title">{this.props.title}</div>
        </div>
    );
  }

}

export default ItemVideo;
