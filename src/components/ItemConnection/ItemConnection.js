import React, { Component } from 'react';
import './ItemConnection.scss';

class ItemConnection extends Component {
  render(){
    return (

      <div className="item-connection">
        <div className="item-connection-img">
          <img src={this.props.img} alt="avatar" />
        </div>
        <div className="item-connection-content">
          <b className="user-name">{this.props.name}</b>
          <span>{this.props.status}</span>
          <span className="text">{this.props.text}</span>
          <span>{this.props.time}</span>
        </div>
      </div>
    );
  }

}

export default ItemConnection;
