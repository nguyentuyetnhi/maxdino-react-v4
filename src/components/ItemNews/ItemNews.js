import React, { Component } from 'react';
import './ItemNews.scss';

class ItemNews extends Component {
  render(){
      return (
          <div className="item-news">
            <div className="item-news-img">
              <img src={this.props.img} />
            </div>
            <div className="item-news-content">
              <div className="item-news-first-child-content">
                <div className="item-news-title">{this.props.title}</div>
                <div className="item-news-detail">{this.props.detail}</div>
              </div>
              <div className="item-news-name">{this.props.name}</div>
            </div>
          </div>
        );
  }

}

export default ItemNews;
