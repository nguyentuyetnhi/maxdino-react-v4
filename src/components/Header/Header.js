import React, { Component } from 'react';
import questionmarkicon from '../../assets/images/question-mark-icon.png';
import searchicon from '../../assets/images/search-icon.png';
import avatarmale from '../../assets/images/avatar-male.png';
import logo from '../../assets/images/logo.png';
import './Header.scss';

class Header extends Component {
  render(){
    return (

      <nav className="navbar navbar-expand-sm navbar-maxdino">
        <a className="navbar-brand" href="#"><img src={logo} alt="logo" /></a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav navbar-nav-left">
            <li className="nav-item dropdown my-2 my-lg-0">
              <div className="dropdown-question-mark">
                <img src={questionmarkicon} className="dropdown-question-mark-icon" />
              </div>
              <select className="dropdown-selecter my-2 my-lg-0">
                <option className="dropdown-item">Ask</option>
                <option className="dropdown-item">Learn</option>
              </select>
            </li>
            <form className="form-inline my-2 my-lg-0">
              <div className="form-inline-search">
                <img src={searchicon}/>
              </div>
              <input className="form-control" type="search" placeholder="Ask something about Finance" aria-label="Search" />
            </form>
          </ul>
          <ul className="navbar-nav ml-auto">
            <li className="nav-item my-2 my-lg-0">
              <a href="#"><span className="icon icon-noti" /></a>
            </li>
            <li className="nav-item dropdown my-2 my-lg-0">
              <div className="avatar-user"><img src={avatarmale} alt="avatar" /></div>
              <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span className="caret" /></a>
              <ul className="dropdown-menu dropdown-menu-right">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    );
  }

}

export default Header;
