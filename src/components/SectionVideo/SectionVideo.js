import React, { Component } from 'react';
import videothumb from '../../assets/images/video-thumb.jpg';
import ItemVideo from '../ItemVideo/ItemVideo.js'
import './SectionVideo.scss';

class SectionVideo extends Component {
  render(){
      return (

        <div className="section section-video">
          <div className="section-title">
            <b>Videos</b>
            <a href="#">See More</a>
          </div>
          <div className="list-item-video">
            <ItemVideo 
              title='Social Media Today'
              img={videothumb}
            />
            
            <ItemVideo 
              title='Social Media Today'
              img={videothumb}
            />

            <ItemVideo 
              title='Social Media Today'
              img={videothumb}
            />
            
          </div>
        </div>
    );
  }

}

export default SectionVideo;
