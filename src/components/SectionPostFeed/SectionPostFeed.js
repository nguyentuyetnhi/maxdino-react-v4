import React, { Component } from 'react';
import avatarmale from '../../assets/images/avatar-male.png';
import imgnews from '../../assets/images/img-news.png';
import './SectionPostFeed.scss';

class SectionPostFeed extends Component {
  render(){
       return (

        <div className="section section-post feed">
          <div className="item-feed">
            <div className="expand-icon float-right">
              <a href="#"><i className="fa fa-ellipsis-h" /></a>
            </div>
            <div className="head-post">
              <div className="avatar-user"><img src={avatarmale} alt="avatar" /></div>
              <div className="head-post-content">
                <div className="head-post-name-user">David Lord</div>
                <div className="head-post-story-subtitle">
                  <div className="date">
                    <span className="icon icon-calendar" />
                    <span className="date-calendar-date">11th Dec, 2017</span>
                  </div>
                  <div className="public">
                    <span className="icon icon-public" />
                    <div className="dropdown">
                      <select className="dropdown-selecter my-2 my-lg-0">
                        <option className="dropdown-item">Everyone</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="content-post">
              <div className="feed-text">So… That’s it</div>
              <div className="post-feed">
                <div className="post-feed-img">
                  <img src={imgnews} alt="feed" />
                </div>
                <div className="post-feed-content">
                  <div className="post-feed-title">Why are China instant noodle sales going off the boil?
                  </div>
                  <div className="post-feed-detail">Be it a snack for students, a meal on the train, or
                    just the go-to choice for hungry workers, more than 46.2 billion packets were
                    sold in China and Hong Kong in 2013.</div>
                  <div className="post-feed-link">
                    <a href="#">http://www.bbc.com/news/business-42390058</a>
                  </div>
                </div>
              </div>
            </div>
            <div className="footer-post post-feed-footer">
              <div className="ratting">
                <span className="icon icon-like" />
                <b>74</b>
                <span className="ratting-name">Reactions</span>
              </div>
              <div className="ratting">
                <span className="icon icon-comment" />
                <b>0</b>
                <span className="ratting-name">Comments</span>
              </div>
            </div>
          </div>
          <div className="item-feed">
            <div className="expand-icon float-right">
              <a href="#"><i className="fa fa-ellipsis-h" /></a>
            </div>
            <div className="head-post">
              <div className="avatar-user"><img src={avatarmale} alt="avatar" /></div>
              <div className="head-post-content">
                <div className="head-post-name-user">David Lord</div>
                <div className="head-post-story-subtitle">
                  <div className="date">
                    <span className="icon icon-calendar" />
                    <span className="date-calendar-date">11th Dec, 2017</span>
                  </div>
                  <div className="public">
                    <span className="icon icon-public" />
                    <div className="dropdown">
                      <select className="dropdown-selecter my-2 my-lg-0">
                        <option className="dropdown-item">Everyone</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="content-post">
              <div className="post-feed-content">
                <div className="post-feed-detail">Be it a snack for students, a meal on the train, or just
                  the go-to choice for hungry workers, more than 46.2 billion packets were sold in
                  China and Hong Kong in 2013.</div>
                <div className="post-feed-link">
                  <a href="#">#china</a>
                  <a href="#">#noddle</a>
                </div>
              </div>
            </div>
            <div className="footer-post post-feed-footer">
              <div className="ratting">
                <span className="icon icon-like" />
                <b>74</b>
                <span className="ratting-name">Reactions</span>
              </div>
              <div className="ratting">
                <span className="icon icon-comment" />
                <b>0</b>
                <span className="ratting-name">Comments</span>
              </div>
            </div>
          </div>
          <div className="item-feed">
            <div className="expand-icon float-right">
              <a href="#"><i className="fa fa-ellipsis-h" /></a>
            </div>
            <div className="head-post">
              <div className="avatar-user"><img src={avatarmale} alt="avatar" /></div>
              <div className="head-post-content">
                <div className="head-post-name-user">David Lord</div>
                <div className="head-post-story-subtitle">
                  <div className="date">
                    <span className="icon icon-calendar" />
                    <span className="date-calendar-date">11th Dec, 2017</span>
                  </div>
                  <div className="public">
                    <span className="icon icon-public" />
                    <div className="dropdown">
                      <select className="dropdown-selecter my-2 my-lg-0">
                        <option className="dropdown-item">Everyone</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="content-post">
              <div className="post-feed-content">
                <div className="post-feed-detail">Be it a snack for students, a meal on the train, or just
                  the go-to choice for hungry workers, more than 46.2 billion packets were sold in
                  China and Hong Kong in 2013.</div>
                <div className="post-feed-link">
                  <a href="#">#china</a>
                  <a href="#">#noddle</a>
                </div>
              </div>
            </div>
            <div className="footer-post post-feed-footer">
              <div className="ratting">
                <span className="icon icon-like" />
                <b>74</b>
                <span className="ratting-name">Reactions</span>
              </div>
              <div className="ratting">
                <span className="icon icon-comment" />
                <b>0</b>
                <span className="ratting-name">Comments</span>
              </div>
            </div>
          </div>
        </div>
      );
  }

}

export default SectionPostFeed;
