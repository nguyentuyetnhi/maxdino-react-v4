import React, { Component } from 'react';
import avatarmale from '../../assets/images/avatar-male.png';
import './SectionConnection.scss';
import ItemConnection from '../ItemConnection/ItemConnection.js'

class SectionConnection extends Component {
  render(){
     return (

        <div className="section section-connection">
          <div className="section-title">
            <b>CONNECTION ACTIVITIES</b>
          </div>
          <div className="list-item-connection">
            <ItemConnection 
              name='David Lord'
              text='“EU tells Netanyahu no support for Trump’s Jerusalem move”'
              time='4h ago'
              img={avatarmale}
            />
            
             <ItemConnection 
              name='David Lord'
              text='“EU tells Netanyahu no support for Trump’s Jerusalem move”'
              time='4h ago'
              img={avatarmale}
            />
             <ItemConnection 
              name='Nhi Nguyen'
              text='“This is good time for us”'
              time='4h ago'
              img={avatarmale}
            />
             <ItemConnection 
              name='Nur Aisha'
              text='“No rest time here guy, fight for best fruit”'
              time='4h ago'
              img={avatarmale}
            />           
          </div>
        </div>
    );    
  }

}

export default SectionConnection;
