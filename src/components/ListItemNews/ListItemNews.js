import React, { Component } from 'react';
import slideitem1 from '../../assets/images/slide-item-1.png';
import slideitem2 from '../../assets/images/slide-item-2.png';
import slideitem3 from '../../assets/images/slide-item-3.png';
import './ListItemNews.scss';
import ItemNews from '../ItemNews/ItemNews.js'

class ListItemNews extends Component {
  render(){
      return (

      <div className="section section-news p-0">
        <div className="container">
          <div className="list-item-news">
            <div className="owl-carousel owl-theme">
              <ItemNews 
                title='What’s Bitcoin?'
                detail='It is a long established fact that a reader will be distracted
                      by the readable content of a page when looking at its layout and something behind…'
                name='David Lord - Finacial Expert'
                img={slideitem1}
              />

              <ItemNews 
                title='Risky Plan to Keep London as Europe’s Financial Hub?'
                detail='It is a long established fact that a reader will be distracted
                      by the readable content…'
                name='David Lord - Chef Officer'
                img={slideitem2}
              />
              
              <ItemNews 
                title='Canadian Leader Justin Trudeau Broke Ethics Laws By Visiting
                      Aga Khan’s Island?'
                detail='IIt is a long established fact that a reader will be distracted
                      by the readable content…'
                name='David Lord - Finacial Expert'
                img={slideitem3}
              />

              <ItemNews 
                title='What’s Bitcoin?'
                detail='It is a long established fact that a reader will be distracted
                      by the readable content of a page when looking at its layout and something behind…'
                name='David Lord - Finacial Expert'
                img={slideitem1}
              />
             
              
              
            </div>
          </div>
        </div>
      </div>
    );
  }

}

export default ListItemNews;
