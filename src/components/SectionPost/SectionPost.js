import React, { Component } from 'react';
import avatarmale from '../../assets/images/avatar-male.png';
import './SectionPost.scss';

class SectionPost extends Component {
  render(){
      
     return (

        <div className="section section-post">
          <div className="head-post">
            <div className="avatar-user"><img src={avatarmale} alt="avatar" /></div>
            <div className="head-post-content">
              <div className="head-post-name-user">David Lord</div>
              <div className="head-post-story-subtitle">
                <div className="date">
                  <span className="icon icon-calendar" />
                  <span className="date-calendar-date">11th Dec, 2017</span>
                </div>
                <div className="public">
                  <span className="icon icon-public" />
                  <div className="dropdown">
                    <select className="dropdown-selecter my-2 my-lg-0">
                      <option className="dropdown-item">Everyone
                      </option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="content-post">
            <div className="form-group">
              <textarea className="form-control" placeholder="Start posting today …" rows={5} defaultValue={""} />
            </div>
          </div>
          <div className="footer-post">
            <div className="row">
              <div className="col-6">
                <div className="footer-post-utility">
                  <a href="#"><span className="icon-camera" /></a>
                  <a href="#"><span className="icon-link" /></a>
                  <a href="#"><span className="icon-add" /></a>
                </div>
              </div>
              <div className="col-6">
                <div className="footer-post-post-on float-right">
                  Also post on
                  <span className="icon icon-icon-facebook blue-icon" />
                  <span className="icon icon-icon-twitter" />
                </div>
              </div>
            </div>
          </div>
        </div>
      );
  }

}

export default SectionPost;
