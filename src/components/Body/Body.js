import React, { Component } from 'react';
import logo from '../../assets/images/logo.png';
import './Body.scss';
import SectionVideo from '../SectionVideo/SectionVideo.js'
import SectionPost from '../SectionPost/SectionPost.js'
import SectionPostFeed from '../SectionPostFeed/SectionPostFeed.js'
import SectionConnection from '../SectionConnection/SectionConnection.js'

class Body extends Component {
  render(){
    return (
      <div className="body">
        <div className="container">
        <div className="row">
          <div className="col-sm-3">
            <SectionVideo />
          </div>
          <div className="col-sm-6">
            <SectionPost />
            <SectionPostFeed />
          </div>
          <div className="col-sm-3">
            <SectionConnection />
          </div>
        </div>
      </div>
      </div>
       
    );
  }

}

export default Body;
